# GIT - the stupid content tracker

"git" can mean anything, depending on your mood.

 - random three-letter combination that is pronounceable, and not
   actually used by any common UNIX command.  The fact that it is a
   mispronounciation of "get" may or may not be relevant.
 - stupid. contemptible and despicable. simple. Take your pick from the
   dictionary of slang.
 - "global information tracker": you're in a good mood, and it actually
   works for you. Angels sing, and a light suddenly fills the room. 
 - "goddamn idiotic truckload of sh*t": when it breaks

## Whaat?

Git is a version control system, i.e. a software that helps software
developers to work together and maintain a complete history of their
work. How ?

 - Allows developers to work simultaneously.
 - Does not allow overwriting each other’s changes.
 - Maintains a history of every version.

Git is a fast, scalable, distributed revision control system with an unusually rich command set that provides both high-level operations and full access to internals.  

Git is a widely used source code management system for
software development. It is a distributed revision control system with
an emphasis on speed, data integrity, and support for
distributed, non-linear workflows. Git was initially designed and
developed in 2005 by Linux kernel developers (including Linus
Torvalds) for Linux kernel development.

As with most other distributed version control systems, and unlike
most client–server systems, every Git working directory is a
full-fledged repository with complete history and full
version-tracking capabilities, independent of network access or a
central server. Like the Linux kernel, Git is free software
distributed under the terms of the GNU General Public License version 2.  

You find many tutorials on the web ! 

For a minimal and sufficient start look at:

     https://git-scm.com/
     http://rogerdudler.github.io/git-guide/
     https://en.wikipedia.org/wiki/Git_%28software%29
     https://en.wikipedia.org/wiki/GitLab

## Quick Start

### (1) Preparations

Make sure you have git installed on your computer. 
Type "git", or "man git" in a terminal: if nothing happens you need to install it. Not difficult, just type:

    $ yum install git  # on Fedora linux
    $ apt-get install git  # on Debian/Ubuntu linux

 Lets assume someone has set up a [git repos](https://git-scm.com/book/it/v2/Git-on-the-Server-Setting-Up-the-Server) 
 for you or that you use some web hosting (GitHub, GitLab, Bitbucket, ...). The only thing you might need to do before 
 starting is to send you SSH public key for authentication. Usually it is stored in the files 

    $ ls ~/.ssh/id_[rd]sa.pub

and it can be created with the command  

    $ ssh-keygen -t rsa

### (2) Clone the repository from the server

    $ git clone [GITUSER]@[MACHINE HOSTNAME]:[REPOSITORY NAME] 

This will create a directory with the repository name in your
working directory and will download the contents of the repo into
it. If this does not happen and instead you are asked to enter a
password, then your ssh key is not loaded. In this case, load your
ssh key like this: 

    $ ssh-add ~/.ssh/id_rsa

Then try again. If this still does not do the trick or if you get
an error message about no "ssh-agent" running, then try 

    $ ssh-agent

This will spit out a few lines of text. Copy them and paste them
back into the prompt, hit enter, then repeat  

    $ ssh-add ~/.ssh/id_rsa

This should do the trick!

### (3) Pulling in changes.

Make sure you have the most recent version of the files before starting to work. 
    
    $ git pull -r 

### (4) Committing and pushing changes.

Whenever you have completed a significant addition or change to
files you can commit them, this way

    $ git add [FILE YOU WERE EDITING]
    $ git commit -m "Your commit message goes here"

Now everything is 'backupped' locally, if you do 

    $ git push
    
you share your work with everybody. Note that you might need to do 

    $ git pull -r  

before pushing,in case someone else has pushed before you the same file. 

Important things:
- commit and push only files that are really needed, e.g. do not commit executable or PDF that can be created from sources 
- do not commit "big" files in case the repos is not for them
- give sensible commit messages so that we others know what you changed

### (5) Looking at the history of changes and checking the status of your repo.

    $ git log
    $ git status
    $ git diff [FILE]

### (6) Adding additional files to the repo.

    $ git add [FILENAME]
    $ git commit -m "Added file [FILENAME] to repo"
    $ git push

### (7) Resolving merge conflicts. 

After adding, committing, and pulling, you might end up with a merge error like 

    ...
    23ae277..b13d53f master -> origin/master Auto-merging [FILE]
    CONFLICT (content): Merge conflict in [FILE]
    Automatic merge failed; fix conflicts and then commit the result.

This means that your local changes and some other changes pushed
to the repos cannot be merged and they are in conflict (e.g. you
and your colaborator edited the same sentence of the initial text
version in two different ways...). Now your file cannot be pushed :( 
You need first to resolve the conflict. In case you're working
with text files, it's easy. Look at the 'corrupted' file, you see parts like
  
    $ cat [FILE] 
    ========= 
    <<<<<<< HEAD 
    this is what is currently in the remote version
    =======
    this is what is currently in your local version
    >>>>>>> b13d53f9cf6a8fe5b58e3c9c103f1dab84026161

git has conveniently inserted information on what the conflict
is. The stuff right beneath `<<<<<< HEAD` is what is currently in
the remote repository and the stuff just below the `=======` is what
we have locally. If your local changes are correct, then you can
simply edit the file and remove the remote stuff and all conflict
stuff around it. Next you git add and git commit the file, then
you execute git push to update the remote with your local
change. This resolves the conflict. 

## Why git ?

Advantages of Git

 - **Free and open source**
Git is released under GPL’s open source license. It is available
freely over the internet. You can use Git to manage propriety projects
without paying a single penny. As it is an open source, you can
download its source code and also perform changes according to your
requirements. 

 - **Fast and small**  
As most of the operations are performed locally, it gives a huge
benefit in terms of speed. Git does not rely on the central server;
that is why, there is no need to interact with the remote server for
every operation. The core part of Git is written in C, which avoids
runtime overheads associated with other high-level languages. Though
Git mirrors entire repository, the size of the data on the client side
is small. This illustrates the efficiency of Git at compressing and
storing data on the client side. 

 - **Implicit backup** 
The chances of losing data are very rare when there are multiple
copies of it. Data present on any client side mirrors the repository,
hence it can be used in the event of a crash or disk corruption. 

 - **Security** 
Git uses a common cryptographic hash function called secure hash
function (SHA1), to name and identify objects within its
database. Every file and commit is check-summed and retrieved by its
checksum at the time of checkout. It implies that, it is impossible to
change file, date, and commit message and any other data from the Git
database without knowing Git. 

 - **No need of powerful hardware** 
In case of centralized version control systems, the central server
needs to be powerful enough to serve requests of the entire team. For
smaller teams, it is not an issue, but as the team size grows, the
hardware limitations of the server can be a performance bottleneck. In
case of distributed version control systems, developers don’t interact
with the server unless they need to push or pull changes. All the
heavy lifting happens on the client side, so the server hardware can
be very simple indeed. 

 - **Easier branching** 
Centralized version control systems use cheap copy mechanism, If we
create a new branch, it will copy all the codes to the new branch, so
it is time-consuming and not efficient. Also, deletion and merging of
branches in centralized version control system is complicated and
time-consuming. But branch management with Git is very simple. It
takes only a few seconds to create, delete, and merge branches.


